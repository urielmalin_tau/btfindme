/******************************************************************************
 * Filename:       bt_service.c
 *
 * Description:    This file contains the implementation of the service.
 *
 *                 Generated by:
 *                 BDS version: 1.0.2093.0
 *                 Plugin:      Texas Instruments CC26xx BLE SDK v2.1 GATT Server plugin 1.0.5 beta
 *                 Time:        Tue Jan 26 2016 22:57:47 GMT+01:00
 *

 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 **********************************************************************************/


/*********************************************************************
 * INCLUDES
 */

#include <string.h>

//#define xdc_runtime_Log_DISABLE_ALL 1  // Add to disable logs from this file
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Diags.h>
#include <driverlib/sys_ctrl.h>

#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>
#include <inc/hw_fcfg1.h>

#include "bcomdef.h"
#include "OSAL.h"
#include "linkdb.h"
#include "att.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"
#include "gapbondmgr.h"

#include "bt_service.h"
#include "util.h"
#include "project_zero.h"

#include "ExtFlash.h"


/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
* GLOBAL VARIABLES
*/

static PIN_State beepPinState;
static PIN_Handle beepPinHandle;
#define L1 IOID_25
PIN_Config beepPinTable[] =

{
L1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
PIN_TERMINATE
};


// BT_Service Service UUID
CONST uint8_t BtServiceUUID[ATT_UUID_SIZE] =
{
  BT_SERVICE_SERV_UUID_BASE128(BT_SERVICE_SERV_UUID)
};

// BT0 UUID
CONST uint8_t ls_BT0UUID[ATT_UUID_SIZE] =
{
  LS_BT_INTERVAL_UUID_BASE128(LS_BT_INTERVAL_UUID)
};

// BT1 UUID
CONST uint8_t ls_BTNameUUID[ATT_UUID_SIZE] =
{
  LS_BT_NAME_UUID_BASE128(LS_BT_NAME_UUID)
};
CONST uint8_t ls_BTBeepUUID[ATT_UUID_SIZE] =
{
  LS_BT_BEEP_UUID_BASE128(LS_BT_BEEP_UUID)
};

CONST uint8_t ls_BTPinUUID[ATT_UUID_SIZE] =
{
  LS_BT_BEEP_UUID_BASE128(LS_BT_PIN_UUID)
};

/*********************************************************************
 * LOCAL VARIABLES
 */
uint16_t interval_mode_to_ticks[BtConfig_INTERVAL_MODE_NUMBER] = {1600, 3200, 4800};
bool is_beeping = 0;
static BtServiceCBs_t *pAppCBs = NULL;

/*********************************************************************
* Profile Attributes - variables
*/

// Service declaration
static CONST gattAttrType_t BtServiceDecl = { ATT_UUID_SIZE, BtServiceUUID };

// Characteristic "BT interval" Properties (for declaration)
static uint8_t ls_BT0Props = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_WRITE_NO_RSP;


// Length of data in characteristic "BTinterval" Value variable, initialized to minimal size.
static uint16_t ls_BT0ValLen = LS_BT_INTERVAL_LEN_MIN;



// Characteristic "BT1" Properties (for declaration)
//static uint8_t ls_BTNameProps = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_WRITE_NO_RSP;
static uint8_t ls_BTNameProps = GATT_PROP_READ | GATT_PROP_WRITE;

// Characteristic "BT1" Value variable

// Length of data in characteristic "BT1" Value variable, initialized to minimal size.
static uint16_t ls_BTNameValLen = LS_BT_NAME_LEN;

// Characteristic "BT BEEP" Properties (for declaration)

static uint8_t ls_BTBeepProps = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_WRITE_NO_RSP;


// Length of data in characteristic "BT BEEP" Value variable, initialized to minimal size.
static uint16_t ls_BTBeepValLen = LS_BT_BEEP_LEN;




// Length of data in characteristic "BT BEEP" Value variable, initialized to minimal size.
static uint16_t ls_BTPinValLen = LS_BT_PIN_LEN;

static uint8_t ls_BTPinProps = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_WRITE_NO_RSP;



/*********************************************************************
* Profile Attributes - Table
*/

static gattAttribute_t BT_ServiceAttrTbl[] =
{
  // BT_Service Service Declaration
  {
    { ATT_BT_UUID_SIZE, primaryServiceUUID },
    GATT_PERMIT_AUTHEN_READ,
    0,
    (uint8_t *)&BtServiceDecl
  },
    // BT0 Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_AUTHEN_READ,
      0,
      &ls_BT0Props
    },
      // BT0 Characteristic Value
      {
        { ATT_UUID_SIZE, ls_BT0UUID },
        GATT_PERMIT_AUTHEN_WRITE | GATT_PERMIT_AUTHEN_READ ,
        0,
		//ls_BT0Val
		(uint8_t*) &btconfig_ctx.interval_mode
      },
    // BT1 Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_AUTHEN_READ,
      0,
      &ls_BTNameProps
    },
      // BT1 Characteristic Value
      {
        { ATT_UUID_SIZE, ls_BTNameUUID },
        GATT_PERMIT_AUTHEN_READ | GATT_PERMIT_AUTHEN_WRITE ,
        0,
		//ls_BTNameVal
		(uint8_t*) btconfig_ctx.ap_name
      },
	    // BT BEEP Characteristic Declaration
	    {
	      { ATT_BT_UUID_SIZE, characterUUID },
	      GATT_PERMIT_AUTHEN_READ,
	      0,
	      &ls_BTBeepProps
	    },
	      // BT BEEP Characteristic Value
	      {
	        { ATT_UUID_SIZE, ls_BTBeepUUID },
	        GATT_PERMIT_AUTHEN_READ | GATT_PERMIT_AUTHEN_WRITE ,
	        0,
			(uint8_t*) &is_beeping
	      },
		  // BT PIN Characteristic Declaration
			{
			  { ATT_BT_UUID_SIZE, characterUUID },
			  GATT_PERMIT_AUTHEN_READ,
			  0,
			  &ls_BTPinProps
			},
			  // BT1 Characteristic Value
			  {
				{ ATT_UUID_SIZE, ls_BTPinUUID },
				GATT_PERMIT_AUTHEN_READ | GATT_PERMIT_AUTHEN_WRITE ,
				0,
				(uint8_t*) &btconfig_ctx.pin_code
			  },
};

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static bStatus_t BT_Service_ReadAttrCB( uint16_t connHandle, gattAttribute_t *pAttr,
                                           uint8_t *pValue, uint16_t *pLen, uint16_t offset,
                                           uint16_t maxLen, uint8_t method );
static bStatus_t BT_Service_WriteAttrCB( uint16_t connHandle, gattAttribute_t *pAttr,
                                            uint8_t *pValue, uint16_t len, uint16_t offset,
                                            uint8_t method );

/*********************************************************************
 * PROFILE CALLBACKS
 */
// Simple Profile Service Callbacks
CONST gattServiceCBs_t BT_ServiceCBs =
{
  BT_Service_ReadAttrCB,  // Read callback function pointer
  BT_Service_WriteAttrCB, // Write callback function pointer
  NULL                       // Authorization callback function pointer
};

/*********************************************************************
* PUBLIC FUNCTIONS
*/

void pretty_print(){
	Log_info5("ap ctx: %x %x %x %x %x" ,*(int*)btconfig_ctx.ap_name,*((int*)btconfig_ctx.ap_name+1),*((int*)btconfig_ctx.ap_name+2),*((int*)btconfig_ctx.ap_name+3),*((int*)btconfig_ctx.ap_name+4));

}

/*
 * BtService_AddService- Initializes the BtService service by registering
 *          GATT attributes with the GATT server.
 *
 *    rspTaskId - The ICall Task Id that should receive responses for Indications.
 */
extern bStatus_t BtService_AddService( uint8_t rspTaskId )
{
  uint8_t status;

  // Register GATT attribute list and CBs with GATT Server App
  status = GATTServApp_RegisterService( BT_ServiceAttrTbl,
                                        GATT_NUM_ATTRS( BT_ServiceAttrTbl ),
                                        GATT_MAX_ENCRYPT_KEY_SIZE,
                                        &BT_ServiceCBs );
  Log_info2("Registered service, %d attributes, status: %d", (IArg)GATT_NUM_ATTRS( BT_ServiceAttrTbl ), (IArg) status);
  return ( status );
}

/*
 * BtService_RegisterAppCBs - Registers the application callback function.
 *                    Only call this function once.
 *
 *    appCallbacks - pointer to application callbacks.
 */
bStatus_t BtService_RegisterAppCBs( BtServiceCBs_t *appCallbacks )
{
  if ( appCallbacks )
  {
    pAppCBs = appCallbacks;
//    Log_info1("Registered callbacks to application. Struct %p", (IArg)appCallbacks);

    return ( SUCCESS );
  }
  else
  {
    //Log_warning0("Null pointer given for app callbacks.");
	  Log_warning0("Null");
    return ( FAILURE );
  }
}

/*
 * BtService_SetParameter - Set a BtService parameter.
 *
 *    param - Profile parameter ID
 *    len   - length of data to write
 *    value - pointer to data to write.  This is dependent on
 *            the parameter ID and may be cast to the appropriate
 *            data type (example: data type of uint16_t will be cast to
 *            uint16_t pointer).
 */
bStatus_t BtService_SetParameter( uint8_t param, uint16_t len, void *value )
{
  bStatus_t ret = SUCCESS;
  uint8_t  *pAttrVal;
  uint16_t *pValLen;
  uint16_t valMinLen;
  uint16_t valMaxLen;

  switch ( param )
  {
    case LS_BT_INTERVAL_ID:
      pAttrVal  =  (uint8_t*) &btconfig_ctx.interval_mode;
      pValLen   = &ls_BT0ValLen;
      valMinLen =  LS_BT_INTERVAL_LEN_MIN;
      valMaxLen =  LS_BT_INTERVAL_LEN;
      Log_info2("SetParameter : %s len: %d", (IArg)"BT0", (IArg)len);
      break;

    case LS_BT_NAME_ID:
      pAttrVal  =  (uint8_t*) btconfig_ctx.ap_name;
      pValLen   = &ls_BTNameValLen;
      valMinLen =  LS_BT_NAME_LEN_MIN;
      valMaxLen =  LS_BT_NAME_LEN;
      Log_info2("SetParameter : %s len: %d", (IArg)"BT1", (IArg)len);
      break;

    case LS_BT_BEEP_ID:
      pAttrVal  = (uint8_t*) &is_beeping;
      pValLen   = &ls_BTBeepValLen;
      valMinLen =  LS_BT_BEEP_LEN_MIN;
      valMaxLen =  LS_BT_BEEP_LEN;
      Log_info2("SetParameter : %s len: %d", (IArg)"BT BEEP", (IArg)len);
      break;
    case LS_BT_PIN_ID:
      pAttrVal  = (uint8_t*) &btconfig_ctx.pin_code;
	  pValLen   = &ls_BTPinValLen;
	  valMinLen =  LS_BT_PIN_LEN_MIN;
	  valMaxLen =  LS_BT_PIN_LEN;
	  Log_info2("SetParameter : %s len: %d", (IArg)"BT PIN", (IArg)len);
	  break;

    default:
      Log_error1("SetParameter: Parameter #%d not valid.", (IArg)param);
      return INVALIDPARAMETER;
  }

  // Check bounds, update value and send notification or indication if possible.
  if ( len <= valMaxLen && len >= valMinLen )
  {
    memcpy(pAttrVal, value, len);
    *pValLen = len; // Update length for read and get.
  }
  else
  {
    Log_error3("Length outside bounds: Len: %d MinLen: %d MaxLen: %d.", (IArg)len, (IArg)valMinLen, (IArg)valMaxLen);
    ret = bleInvalidRange;
  }

  return ret;
}


/*
 * BtService_GetParameter - Get a BtService parameter.
 *
 *    param - Profile parameter ID
 *    len   - pointer to a variable that contains the maximum length that can be written to *value.
              After the call, this value will contain the actual returned length.
 *    value - pointer to data to write.  This is dependent on
 *            the parameter ID and may be cast to the appropriate
 *            data type (example: data type of uint16_t will be cast to
 *            uint16_t pointer).
 */
bStatus_t BtService_GetParameter( uint8_t param, uint16_t *len, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
    case LS_BT_INTERVAL_ID:
      *len = MIN(*len, ls_BT0ValLen);
      memcpy(value, &btconfig_ctx.interval_mode, *len);
      Log_info2("GetParameter : %s returning %d bytes", (IArg)"BT0", (IArg)*len);
      break;

    case LS_BT_NAME_ID:
      *len = MIN(*len, ls_BTNameValLen);
      memcpy(value, btconfig_ctx.ap_name, *len);
      Log_info2("GetParameter : %s returning %d bytes", (IArg)"BT1", (IArg)*len);
      break;

    case LS_BT_BEEP_ID:
          *len = MIN(*len, ls_BTBeepValLen);
         memcpy(value, &is_beeping, *len);
          Log_info2("GetParameter : %s returning %d bytes", (IArg)"BT Beep", (IArg)*len);
          break;

    case LS_BT_PIN_ID:
          *len = MIN(*len, ls_BTPinValLen);
         memcpy(value, &btconfig_ctx.pin_code, *len);
          Log_info2("GetParameter : %s returning %d bytes", (IArg)"BT Pin", (IArg)*len);
          break;

    default:
      Log_error1("GetParameter: Parameter #%d not valid.", (IArg)param);
      ret = INVALIDPARAMETER;
      break;
  }
  return ret;
}

/*********************************************************************
 * @internal
 * @fn          BT_Service_findCharParamId
 *
 * @brief       Find the logical param id of an attribute in the service's attr table.
 *
 *              Works only for Characteristic Value attributes and
 *              Client Characteristic Configuration Descriptor attributes.
 *
 * @param       pAttr - pointer to attribute
 *
 * @return      uint8_t paramID (ref bt_service.h) or 0xFF if not found.
 */
static uint8_t BT_Service_findCharParamId(gattAttribute_t *pAttr)
{
  // Is this a Client Characteristic Configuration Descriptor?
  if (ATT_BT_UUID_SIZE == pAttr->type.len && GATT_CLIENT_CHAR_CFG_UUID == *(uint16_t *)pAttr->type.uuid)
    return BT_Service_findCharParamId(pAttr - 1); // Assume the value attribute precedes CCCD and recurse

  // Is this attribute in "BT0"?
  else if ( ATT_UUID_SIZE == pAttr->type.len && !memcmp(pAttr->type.uuid, ls_BT0UUID, pAttr->type.len))
    return LS_BT_INTERVAL_ID;

  // Is this attribute in "BT1"?
  else if ( ATT_UUID_SIZE == pAttr->type.len && !memcmp(pAttr->type.uuid, ls_BTNameUUID, pAttr->type.len))
    return LS_BT_NAME_ID;
    // Is this attribute in "BT BEEP"?
  else if ( ATT_UUID_SIZE == pAttr->type.len && !memcmp(pAttr->type.uuid, ls_BTBeepUUID, pAttr->type.len))
      return LS_BT_BEEP_ID;
  else if ( ATT_UUID_SIZE == pAttr->type.len && !memcmp(pAttr->type.uuid, ls_BTPinUUID, pAttr->type.len))
        return LS_BT_PIN_ID;

  else
    return 0xFF; // Not found. Return invalid.
}

/*********************************************************************
 * @fn          BT_Service_ReadAttrCB
 *
 * @brief       Read an attribute.
 *
 * @param       connHandle - connection message was received on
 * @param       pAttr - pointer to attribute
 * @param       pValue - pointer to data to be read
 * @param       pLen - length of data to be read
 * @param       offset - offset of the first octet to be read
 * @param       maxLen - maximum length of data to be read
 * @param       method - type of read message
 *
 * @return      SUCCESS, blePending or Failure
 */
static bStatus_t BT_Service_ReadAttrCB( uint16_t connHandle, gattAttribute_t *pAttr,
                                       uint8_t *pValue, uint16_t *pLen, uint16_t offset,
                                       uint16_t maxLen, uint8_t method )
{
  bStatus_t status = SUCCESS;
  uint16_t valueLen;
  uint8_t paramID = 0xFF;

  // Find settings for the characteristic to be read.
  paramID = BT_Service_findCharParamId( pAttr );
  switch ( paramID )
  {
    case LS_BT_INTERVAL_ID:
      valueLen = ls_BT0ValLen;

      Log_info4("ReadAttrCB : %s connHandle: %d offset: %d method: 0x%02x",
                 (IArg)"BT0",
                 (IArg)connHandle,
                 (IArg)offset,
                 (IArg)method);
      /* Other considerations for BT0 can be inserted here */
      break;

    case LS_BT_NAME_ID:
      valueLen = ls_BTNameValLen;

      Log_info4("ReadAttrCB : %s connHandle: %d offset: %d method: 0x%02x",
                 (IArg)"BT1",
                 (IArg)connHandle,
                 (IArg)offset,
                 (IArg)method);
      /* Other considerations for BT1 can be inserted here */
      break;

    case LS_BT_BEEP_ID:
      valueLen = ls_BTBeepValLen;

      Log_info4("ReadAttrCB : %s connHandle: %d offset: %d method: 0x%02x",
                 (IArg)"BT BEEP",
                 (IArg)connHandle,
                 (IArg)offset,
                 (IArg)method);
      /* Other considerations for BT1 can be inserted here */
      break;

    case LS_BT_PIN_ID:
      valueLen = ls_BTPinValLen;

      Log_info4("ReadAttrCB : %s connHandle: %d offset: %d method: 0x%02x",
                 (IArg)"BT PIN",
                 (IArg)connHandle,
                 (IArg)offset,
                 (IArg)method);
      /* Other considerations for BT1 can be inserted here */
      break;

    default:
      Log_error0("Attribute was not found.");
      return ATT_ERR_ATTR_NOT_FOUND;
  }
  // Check bounds and return the value
  if ( offset > valueLen )  // Prevent malicious ATT ReadBlob offsets.
  {
    Log_error0("An invalid offset was requested.");
    status = ATT_ERR_INVALID_OFFSET;
  }
  else
  {
    *pLen = MIN(maxLen, valueLen - offset);  // Transmit as much as possible
    memcpy(pValue, pAttr->pValue + offset, *pLen);
    if(paramID == LS_BT_PIN_ID){
		uint8 temp = *pValue;
		*pValue = *(pValue + 2);
		*(pValue + 2) = temp;
    }
    Log_info0("Read value:");
    pValue[*pLen] = '\0';
    Log_info0(pValue);
  }

  return status;
}

/*********************************************************************
 * @fn      BT_Service_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 * @param   method - type of write message
 *
 * @return  SUCCESS, blePending or Failure
 */
static bStatus_t BT_Service_WriteAttrCB( uint16_t connHandle, gattAttribute_t *pAttr,
                                        uint8_t *pValue, uint16_t len, uint16_t offset,
                                        uint8_t method )
{
  bStatus_t status  = SUCCESS;
  uint8_t   paramID = 0xFF;
  uint8_t   changeParamID = 0xFF;
  uint16_t writeLenMin;
  uint16_t writeLenMax;
  uint16_t *pValueLenVar;
  uint8_t val;
  // Find settings for the characteristic to be written.
  paramID = BT_Service_findCharParamId( pAttr );
  switch ( paramID )
  {
    case LS_BT_INTERVAL_ID:
      writeLenMin  = LS_BT_INTERVAL_LEN_MIN;
      writeLenMax  = LS_BT_INTERVAL_LEN;
      pValueLenVar = &ls_BT0ValLen;

      /*Log_info5("WriteAttrCB : %s connHandle(%d) len(%d) offset(%d) method(0x%02x)",
                 (IArg)"BT0",
                 (IArg)connHandle,
                 (IArg)len,
                 (IArg)offset,
                 (IArg)method); */
      /* Other considerations for BT0 can be inserted here */

      break;

    case LS_BT_NAME_ID:
      writeLenMin  = LS_BT_NAME_LEN_MIN;
      writeLenMax  = LS_BT_NAME_LEN;
      pValueLenVar = &ls_BTNameValLen;

      Log_info5("WriteAttrCB : %s connHandle(%d) len(%d) offset(%d) method(0x%02x)",
                 (IArg)"BT1",
                 (IArg)connHandle,
                 (IArg)len,
                 (IArg)offset,
                 (IArg)method);
      /* Other considerations for BT1 can be inserted here */
      break;

    case LS_BT_BEEP_ID:
      writeLenMin  = LS_BT_BEEP_LEN_MIN;
      writeLenMax  = LS_BT_BEEP_LEN;
      pValueLenVar = &ls_BTBeepValLen;

      Log_info5("WriteAttrCB : %s connHandle(%d) len(%d) offset(%d) method(0x%02x)",
                 (IArg)"BT BEEP",
                 (IArg)connHandle,
                 (IArg)len,
                 (IArg)offset,
                 (IArg)method);
      /* Other considerations for BT1 can be inserted here */
      break;

    case LS_BT_PIN_ID:
      writeLenMin  = LS_BT_PIN_LEN_MIN;
      writeLenMax  = LS_BT_PIN_LEN;
      pValueLenVar = &ls_BTPinValLen;

      Log_info5("WriteAttrCB : %s connHandle(%d) len(%d) offset(%d) method(0x%02x)",
                 (IArg)"BT PIN",
                 (IArg)connHandle,
                 (IArg)len,
                 (IArg)offset,
                 (IArg)method);
      /* Other considerations for BT1 can be inserted here */
      break;

    default:
      Log_error0("Attribute was not found.");
      return ATT_ERR_ATTR_NOT_FOUND;
  }
  // Check whether the length is within bounds.
  if ( offset >= writeLenMax )
  {
    Log_error0("An invalid offset was requested.");
    status = ATT_ERR_INVALID_OFFSET;
  }
  else if ( offset + len > writeLenMax )
  {
    Log_error0("Invalid value length was received.");
    status = ATT_ERR_INVALID_VALUE_SIZE;
  }
  else if ( offset + len < writeLenMin && ( method == ATT_EXECUTE_WRITE_REQ || method == ATT_WRITE_REQ ) )
  {
    // Refuse writes that are lower than minimum.
    // Note: Cannot determine if a Reliable Write (to several chars) is finished, so those will
    //       only be refused if this attribute is the last in the queue (method is execute).
    //       Otherwise, reliable writes are accepted and parsed piecemeal.
    Log_error0("Invalid value length was received.");
    status = ATT_ERR_INVALID_VALUE_SIZE;
  }
  else
  {

	  // Copy pValue into the variable we point to from the attribute table.


		// Only notify application and update length if enough data is written.
		//
		// Note: If reliable writes are used (meaning several attributes are written to using ATT PrepareWrite),
		//       the application will get a callback for every write with an offset + len larger than _LEN_MIN.
		// Note: For Long Writes (ATT Prepare + Execute towards only one attribute) only one callback will be issued,
		//       because the write fragments are concatenated before being sent here.
		if ( offset + len >= writeLenMin )
		{
		  changeParamID = paramID;
		  *pValueLenVar = offset + len; // Update data length.
		}
		switch(paramID){
			case LS_BT_NAME_ID:
				/*for(int i=0;i<len;i++)
					btconfig_ctx.ap_name[i] =  pValue[i];
					*/
				btconfig_ctx.ap_name[len] = '\0';
				break;
			case LS_BT_INTERVAL_ID:
				val = *(uint8_t*) pValue;
				if(val >= BtConfig_INTERVAL_MODE_NUMBER){
					status = ATT_ERR_INVALID_VALUE;
					Log_error1("Bad interval mode (0-%d)", BtConfig_INTERVAL_MODE_NUMBER - 1);
					return status;
				}
				//btconfig_ctx.interval_mode = val;
				break;
			case LS_BT_BEEP_ID:
				val = *(uint8*) pValue;
				if(val != 0 && val != 1)
				{
					status = ATT_ERR_INVALID_VALUE;
					Log_error0("Bad beep number (0 or 1)");
					return status;

				}
				break;
			case LS_BT_PIN_ID:
				uint8 temp = *pValue;
				*pValue = *(pValue + 2);
				*(pValue + 2) = temp;
				uint32_t temp_val = *(uint32_t *) pValue & 0x00FFFFFF;
				if(temp_val > 999999){
					status = ATT_ERR_INVALID_VALUE;
					Log_error0("Bad PIN code (0-999999)");
					return status;
				}
				break;
		}
		memcpy(pAttr->pValue + offset, pValue, len);
		if (paramID==LS_BT_NAME_ID || paramID==LS_BT_INTERVAL_ID || paramID == LS_BT_PIN_ID)
		{
			BtConfig_write();
			/*bool valid_ctx = BtConfig_read();
			if(valid_ctx)
			{
				pretty_print();
				Log_info4("Magic: %x, int: %d, name: %s, len:%d", (IArg) btconfig_ctx.magic, (IArg) btconfig_ctx.interval_mode, (IArg) btconfig_ctx.ap_name, (IArg) strlen(btconfig_ctx.ap_name));
			}
			else
				Log_info0("Read failed");
			*/
			SysCtrlSystemReset();
		}
		else if(paramID==LS_BT_BEEP_ID)
			set_beep(is_beeping);

	}

  // Let the application know something changed (if it did) by using the
  // callback it registered earlier (if it did).
  if (changeParamID != 0xFF)
    if ( pAppCBs && pAppCBs->pfnChangeCb )
      pAppCBs->pfnChangeCb( connHandle, BT_SERVICE_SERV_UUID, paramID, pValue, len+offset ); // Call app function from stack task context.

  return status;
}

extern void BtConfig_close(){
	return ExtFlash_close();
}

extern bool BtConfig_open()
{
	return ExtFlash_open();
}

extern bool BtConfig_read()
{
		if(!ExtFlash_read(BtConfig_OFFSET, sizeof(BtConfig), (uint8_t *)&btconfig_ctx))
	{
		//Log_info0("read failed");
		ExtFlash_close();
		return false;
	}
	if(btconfig_ctx.magic != (uint32)BtConfig_MAGIC)
	{
		Log_info1("Magic was corrupted %x",BtConfig_MAGIC);
		return false;
	}
	pretty_print();
	return true;
}

extern void BtConfig_set_default_values(){
	char *mac_addr;
	uint8 *mac_addr_ptr =  ((uint8 *)(FCFG1_BASE + FCFG1_O_MAC_BLE_0));
	mac_addr = Util_convertBdAddr2Str(mac_addr_ptr) + 2;
	btconfig_ctx.magic = (uint32) BtConfig_MAGIC;
	btconfig_ctx.interval_mode = BtConfig_DEFAULT_INTERVAL_MODE;
	strcpy(btconfig_ctx.ap_name,  BtConfig_DEFAULT_PREFIX);
	strcat(btconfig_ctx.ap_name, mac_addr);
	btconfig_ctx.pin_code = BtConfig_DEFAULT_PIN_CODE;
	Log_info1("Default Name %s", (IArg) btconfig_ctx.ap_name);
	BtConfig_write();
}
extern void BtConfig_factory_reset(){
	BtConfig_set_default_values();
	SysCtrlSystemReset();
}
extern void BtConfig_init(){
    beepPinHandle = PIN_open(&beepPinState, beepPinTable);

	bool valid_ctx = false;
	if(!BtConfig_open())
	{
		//Log_info0("couldnt open");
	}
	valid_ctx = BtConfig_read();
	Log_info5("Magic: %x, int: %d, name: %s, len:%d, PIN :%d", (IArg) btconfig_ctx.magic, (IArg) btconfig_ctx.interval_mode, (IArg) btconfig_ctx.ap_name, (IArg) strlen(btconfig_ctx.ap_name), (IArg) btconfig_ctx.pin_code);
	if(!valid_ctx)
	{
		BtConfig_factory_reset();
	}
	else
		Log_info0("Valid btconfig ctx");
	BtService_SetParameter(LS_BT_NAME_ID, strlen(btconfig_ctx.ap_name), btconfig_ctx.ap_name);


}

extern bool BtConfig_write()
{
	if(!ExtFlash_erase(BtConfig_OFFSET, sizeof(BtConfig))){
		return false;
	}
	if(!ExtFlash_write(BtConfig_OFFSET, sizeof(BtConfig), (uint8_t *)&btconfig_ctx))
	{
		//Log_info0("write failed");
		ExtFlash_close();
		return false;
	}
	Log_info0("write succeeded");
	return true;
}
extern BtConfig* get_btconfig(){
	return &btconfig_ctx;
}
void set_beep(bool value)
{
	Log_info1("Beep: %s" , (IArg)  (value ? "start" : "stop"));
    PIN_setOutputValue(beepPinHandle, IOID_25, value);

}

extern uint16_t BtConfig_translate_interval_mode_to_interval(uint8_t mode){
	return interval_mode_to_ticks[mode];
}
/*
static uint16_t read_saved_interval(){
	return 1;
}
static void write_saved_interval(uint16_t new_val){
}

static char* read_ap_name(){
	return null;
}

static void write_ap_name(){
}

static void reboot(){
}
}*/

